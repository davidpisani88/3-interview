<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('/css/styles.css') }}">
</head>
<body>

<div id="header" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <p id="headerLogo">DPQuotes</p>
            </div>
            <div class="col-md-7 text-right">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#dbClearCheck">
                    Recreate database
                </button> |
                <a href="{{ url('/logout') }}">Logout</a></div>
        </div>
    </div>
</div>

@yield('content')


<div id="dbClearCheck" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Database clear?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to clear the database.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary clearDbFinal">Yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<script>

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/643123a18b.js"></script>
<script type="text/javascript" src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="{{ asset('/js/bootstrap-typeahead.js') }}"></script>
<script src="{{ asset('/js/scripts.js') }}"></script>

<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '1075061592700512',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.9'
        });

        FB.AppEvents.logPageView();


        $('.btn-facebook').click(function (e) {
            e.preventDefault();
            FB.login(function(response) {
                if (response.authResponse) {
                    FB.api('/me', function(response) {
                        $.ajax({
                            url: '/getUser',
                            type: 'POST',
                            data: 'name='+response.name+'&fb_id='+response.id,
                            dataType: "json",
                            success: function(data) {
                                location.href = '/'
                            },
                            error: function(data) {
                            }
                        });

                        $('.signedOut').hide();
                        $('.signedIn').show();
                        $('.userName').text(response.name)
                    });
                } else {
                    // console.log('User cancelled login or did not fully authorize.');
                }
            });
        });
    };

</script>
</body>
</html>
