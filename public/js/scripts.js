
$( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#button-addon2').click(function(e){

        $('#button-addon2').text('Searching...').prop('disabled', true);

        var stockSymbol = $('#stockSymbol').val();

        if(stockSymbol == ''){
            $('#stockSymbol').addClass('is-invalid');
            $('.input-error').text('Input cannot be empty, please enter a valid stock symbol.').show();
            $('#button-addon2').text('Get Price').prop('disabled', false);
            return false;
        }

        $.ajax({
            url: '/getLatestPrice',
            type: 'GET',
            data: 'stockSymbol='+stockSymbol,
            dataType: "json",
            success: function(data) {
                $('#stockSymbol').removeClass('is-invalid');
                $('.input-error').hide();
                $('#stockData').html(data);
                $('#button-addon2').text('Get Price').prop('disabled', false);
            },
            error: function(data) {
                $('#stockSymbol').addClass('is-invalid');
                $('.input-error').text('This stock does not exist, please enter a valid symbol.').show();
                $('#button-addon2').text('Get Price').prop('disabled', false);
            }
        });
    });


    $('.clearDbFinal').click(function(){
        $.ajax({
            url: '/migrateReset',
            type: 'GET',
            data: '',
            dataType: "json",
            success: function(data) {
                location.href = '/';
            },
            error: function(data) {
                location.href = '/';
            }
        });
    });



});