<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as User;
use App\Quote as Quote;
use Illuminate\Support\Facades\Auth;

class ApiController extends Controller
{

    public function getUser(Request $request){

        $data = $request->input();
        $user = User::store($data);

        if($user){
            Auth::login($user);
            $loggeduser = Auth::user();

            return json_encode($loggeduser);
        }
    }

    public function getLatestPrice(Request $request){

        $data = file_get_contents('https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol='.$request->input('stockSymbol').'&apikey=0O18XUJW9P8QVGQJ');

        if($data){
            $json = json_decode($data, true);
            $newQuote = Quote::store($json['Global Quote']);
            $returnHTML = view('stockData')->with('data', $newQuote)->render();

            return response()->json($returnHTML);
        }
    }

    public function migrateReset(){
        \Artisan::call('migrate:reset');
        \Artisan::call('migrate');
    }

    public function checkSession(Request $request){

        $data = $request->session()->all();
        $loggeduser = Auth::user();
        dd($data,$loggeduser);
    }
}
