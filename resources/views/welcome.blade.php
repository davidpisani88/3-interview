@extends('template')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(!Auth::user())
            <div id="loginScreen" class="screen signedOut">
                <div id="fbBtn" class="container">
                    <div class="btn btn-lg btn-social btn-facebook">
                        <i class="fa fa-facebook fa-fw"></i> Sign in with Facebook
                    </div>
                </div>

            </div>
            @else

            <div id="welcomeScreen" class="signedIn">
                <p class="align-center">Welcome <span class="userName">{{Auth::user()->name}}</span></p>
                <div class="input-group mb-3">
                    <input id="stockSymbol" type="text" class="form-control" placeholder="Enter a stock symbol (i.e. AMZN)" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="button-addon2">Get Price</button>
                    </div>
                </div>
                <div class="input-error alert alert-danger">Input cannot be empty, please enter a valid stock symbol.</div>
                <div style="clear: both;"></div>
                <div id="stockData">
                  @include('stockData')
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@stop
