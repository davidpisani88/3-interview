<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Quote extends Model
{

    protected $fillable = ['symbol'];

    public static function store($data){

        $newQuote = new Quote;
        $newQuote->fb_id = Auth::user()->fb_id;
        $newQuote->quote_symbol = $data['01. symbol'];
        $newQuote->high = $data['03. high'];
        $newQuote->low = $data['04. low'];
        $newQuote->price = $data['05. price'];

        $newQuote->save();

        $all = Quote::where('fb_id', Auth::user()->fb_id)->get()->sortByDesc('id');

        return $all;
    }
}
