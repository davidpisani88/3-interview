<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Quote Symbol</th>
        <th scope="col">High</th>
        <th scope="col">Low</th>
        <th scope="col">Price</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $i=>$d)
        <tr @if($d->id === count($data)) class="table-success" @endif>
            <th scope="row">{{$d->id}}</th>
            <td>{{$d->quote_symbol}}</td>
            <td>{{$d->high}}</td>
            <td>{{$d->low}}</td>
            <td>{{$d->price}}</td>
        </tr>
    @endforeach
    </tbody>
</table>