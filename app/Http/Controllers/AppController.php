<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Quote as Quote;

class AppController extends Controller
{
    public function index()
    {

        $quotes = [];

        if(Auth::user()){
            $quotes = Quote::where('fb_id', Auth::user()->fb_id)->get()->sortByDesc('id');
        }

        return view('welcome')->with('data', $quotes);
    }
}
